package LongitudeAndLatitude;

import Exceptions.AccuracyIsNotPossibleException;
import Exceptions.CoordinatesAreNotPossibleException;
import Exceptions.DirectionNotFoundException;
import Exceptions.PoleNotFoundException;

import java.util.Arrays;
import java.util.List;

/** Широта и долгота
 */
public final class LongitudeAndLatitude {

    /* =========================== Свойства =============================== */

    /* ----------------------- Широта и долгота --------------------------- */
    private double longitude;
    private String longitudeDir;
    private double latitude;
    private String latitudeDir;

    public List<Object> getLongitude() { return Arrays.asList(longitude, longitudeDir); }
    public List<Object> getLatitude() { return Arrays.asList(latitude, latitudeDir); }

    public double getLongitudeDegreesOnly() { return longitude; }
    public double getLatitudeDegreesOnly() { return latitude; }

    /* ---------------- Предопределённые широта и долгота ----------------- */

    /** Предопределённая координатная точка Волгограда
     *  @return LongitudeAndLatitude - новая координатная точка с долготой и широтой Волгограда
     */
    public static LongitudeAndLatitude Volgograd() {
        return  new LongitudeAndLatitude(44.5168022, "East", 48.7106765, "North");
    }

    /** Предопределённая координатная точка Москвы
     *  @return LongitudeAndLatitude - новая координатная точка с долготой и широтой Москвы
     */
    public static LongitudeAndLatitude Moscow() {
        return  new LongitudeAndLatitude(37.61556, "East", 55.75222, "North");
    }

    /** Предопределённая координатная точка Северного полюса
     *  @return LongitudeAndLatitude - новая координатная точка с долготой и широтой Северного полюса
     */
    public static LongitudeAndLatitude NorthPole() {
        return  new LongitudeAndLatitude("North");
    }

    /** Предопределённая координатная точка Южного полюса
     *  @return LongitudeAndLatitude - новая координатная точка с долготой и широтой Южного полюса
     */
    public static LongitudeAndLatitude SouthPole() {
        return  new LongitudeAndLatitude("South");
    }

    /* =========================== Операции =============================== */

    /* ---------------------------- Порождение ---------------------------- */

    /** Создание координатной точки с указанием ее долготы и широты со сторонами света в качестве знака.
     *  Для удобства расчёта широте и долготе присваивается конкретный знак, который, при необходимости, автоматически настраивается основываясь на указанных сторонах света (Плюс - North/East, Минус - South/West)
     *  При введении некорректной стороны света выводится ошибка DirectionNotFoundException
     *  При введении некорректных координат выводится ошибка CoordinatesAreNotPossibleException
     *  @param longitude - долгота, представляется в градусах в виде десятичной дроби
     *  @param longitudeDir - знак долготы в виде стороны света, представляется в виде строки одним словом с большой буквы (Плюс - East, Минус - West)
     *  @param latitude - широта, представляется в градусах в виде десятичной дроби
     *  @param latitudeDir - знак широты в виде стороны света, представляется в виде строки одним словом с большой буквы (Плюс - North, Минус - South)
     */
    public LongitudeAndLatitude(double longitude, String longitudeDir, double latitude, String latitudeDir){
        if(!longitudeDir.matches("East|West")){
            throw new DirectionNotFoundException("No such direction as " + longitudeDir + " found for longitude");
        }
        else if(!latitudeDir.matches("North|South")){
            throw new DirectionNotFoundException("No such direction as " + latitudeDir + " found for latitude");
        }
        else if(longitude > 180 || longitude < -180){
            throw new CoordinatesAreNotPossibleException("Longitude can't be bigger than 180 degrees or lesser than -180 degrees");
        }
        else if(latitude > 90 || latitude < -90){
            throw new CoordinatesAreNotPossibleException("Latitude can't be bigger than 90 degrees or lesser than -90 degrees");
        }
        else{
            boolean isEast = longitudeDir.equals("East");
            boolean isWest = longitudeDir.equals("West");
            if(isEast && (longitude <= 0 && longitude >= -180) || isWest && (longitude >= 0 && longitude <= 180)) {
                longitude *= -1;
            }

            boolean isNorth = latitudeDir.equals("North");
            boolean isSouth = latitudeDir.equals("South");
            if(isNorth && (latitude <= 0 && latitude >= -90) || isSouth && (latitude >= 0 && latitude <= 90)) {
                latitude *= -1;
            }

            this.longitude = longitude;
            this.longitudeDir = longitudeDir;
            this.latitude = latitude;
            this.latitudeDir = latitudeDir;
        }
    }

    /** Создание координатной точки с указанием ее долготы и широты со знаком без сторон света.
     *  Стороны света автоматически настраивается основываясь на указанных широте и долготе
     *  При введении некорректных координат выводится ошибка CoordinatesAreNotPossibleException
     *  @param longitude - долгота, представляется в градусах в виде десятичной дроби
     *  @param latitude - широта, представляется в градусах в виде десятичной дроби
     */
    public LongitudeAndLatitude(double longitude, double latitude){
        if(longitude > 180 || longitude < -180){
            throw new CoordinatesAreNotPossibleException("Longitude can't be bigger than 180 degrees or lesser than -180 degrees");
        }
        else if(latitude > 90 || latitude < -90){
            throw new CoordinatesAreNotPossibleException("Latitude can't be bigger than 180 degrees or lesser than -180 degrees");
        }

        boolean isEast = longitude >= 0 && longitude <= 180;
        if(isEast) {
            this.longitudeDir = "East";
        }
        else {
            this.longitudeDir = "West";
        }

        boolean isNorth = latitude >= 0 && latitude <= 90;
        if(isNorth) {
            this.latitudeDir = "North";
        }
        else {
            this.latitudeDir = "South";
        }

        this.longitude = longitude;
        this.latitude = latitude;
    }

    /** Создание координатной точки северного или южного полюса с указанием этого полюса.
     *  У полюсов отстутствует долгота, в связи с чем для удобства расчётов долготе присваивается 0, а стороне света Null в виде строки
     *  @param pole - наименование нужного полюса, представляется в виде строки одним словом с большой буквы (North, South)
     */
    private LongitudeAndLatitude(String pole) {
        if(pole.matches("North")) {
            this.longitude = 0;
            this.longitudeDir = "Null";
            this.latitude = 90;
            this.latitudeDir = pole;
        }
        else if(pole.matches("South")) {
            this.longitude = 0;
            this.longitudeDir = "Null";
            this.latitude = -90;
            this.latitudeDir = pole;
        }
        else {
            throw new PoleNotFoundException(pole + " pole not found");
        }
    }

    /* --------------------- Операции взаимодействия ---------------------- */

    /** Рассчёт расстояния между двумя координатными точками.
     *  Поскольку радиус Земли разнится из-за её не совершенной формы шара, в расчете берется среднее его значение - 6371.008 км (даёт 0.5% ошибки расчёта)
     *  @param LAL - координатная точка, до которой рассчитывается расстояние
     *  @return double - расстояние между двумя заданными координатными точками в км
     */
    public double getDistanceFrom(LongitudeAndLatitude LAL){
        double EarthRad = 6371.008;
        double dLon = Math.toRadians(LAL.getLongitudeDegreesOnly() - this.longitude);
        double dLat = Math.toRadians(LAL.getLatitudeDegreesOnly() - this.latitude);

        double c = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(this.latitude)) * Math.cos(Math.toRadians(LAL.getLatitudeDegreesOnly())) * Math.sin(dLon/2) * Math.sin(dLon/2);
        double dSin = 2 * Math.atan2(Math.sqrt(c), Math.sqrt(1-c));

        return EarthRad * dSin;
    }

    /** Рассчёт новой координатной точки относительно заданной координатной точки на заданном расстоянии в заданном направлении.
     *  Поскольку радиус Земли разнится из-за её не совершенной формы шара, в расчете берется среднее его значение - 6371.008 км (даёт 0.5% ошибки расчёта)
     *  @param distance - расстояние от заданной координатной точки в км
     *  @param bearing - горизонтальный угол между настоящим севером и необходимым направлением с центром в заданной координатной точке
     *  @return LongitudeAndLatitude - найденная в заданном направлении на заданном расстоянии новая координатная точка
     */
    public LongitudeAndLatitude getLonAndLatByDistance(double distance, double bearing){
        double EarthRad = 6371.008;

        bearing = Math.toRadians(bearing);
        double lat1 = Math.toRadians(this.latitude);
        double lon1 = Math.toRadians(this.longitude);
        double dR = distance / EarthRad;

        double a = Math.sin(dR) * Math.cos(lat1);
        double lat2 = Math.asin(Math.sin(lat1) * Math.cos(dR) + a * Math.cos(bearing));
        double lon2 = lon1 + Math.atan2(Math.sin(bearing) * a, Math.cos(dR) - Math.sin(lat1) * Math.sin(lat2));

        return new LongitudeAndLatitude(Math.toDegrees(lon2), Math.toDegrees(lat2));
    }

    /** Определение попадания в указанную координатную точку с заданной точностью.
     *  @param LAL - координатная точка, попадание в которую необходимо проверить
     *  @param accuracy - точность попадания, задаётся в виде % от 0 до 100
     *  @return boolean - true - если попадание в указанную координатную точку истинно, false  - если попадание в указанную координатную точку ложно
     */
    public boolean isHittingPosition(LongitudeAndLatitude LAL, double accuracy) {
        if (accuracy < 0 || accuracy > 100)
            throw new AccuracyIsNotPossibleException("Accuracy " + accuracy + " is not possible");

        accuracy = 100 - accuracy;

        double lonDif = accuracy * (LAL.getLongitudeDegreesOnly() / 100);
        double latDif = accuracy * (LAL.getLatitudeDegreesOnly() / 100);

        double lonMax = LAL.getLongitudeDegreesOnly() + lonDif;
        double lonMin = LAL.getLongitudeDegreesOnly() - lonDif;

        double latMax = LAL.getLatitudeDegreesOnly() + latDif;
        double latMin = LAL.getLatitudeDegreesOnly() - latDif;

        return (this.longitude <= lonMax && this.longitude >= lonMin && this.latitude <= latMax && this.latitude >= latMin);
    }

    /** Определение попадания в указанные координаты с заданной точностью.
     *  @param longitude - указанная для проверки попадания долгота
     *  @param latitude - указанная для проверки попадания широта
     *  @param accuracy - точность попадания, задаётся в виде % от 0 до 100
     *  @return boolean - true - если попадание в указанную координатную точку истинно, false  - если попадание в указанную координатную точку ложно
     */
    public boolean isHittingPosition(double longitude, double latitude, double accuracy) {
        LongitudeAndLatitude LAL = new LongitudeAndLatitude(longitude, latitude);

        return this.isHittingPosition(LAL, accuracy);
    }

    /* --------------------- Операции преобразования ---------------------- */

    /** Представить координатную точку как строку.
     *  У полюсов отстутствует долгота, в связи с чем при преобразовании полюса в строку долгота не выводится
     *  @return String - строковое представление координатной точки
     */
    @Override
    public String toString()
    {
        String s;

        if(!this.longitudeDir.equals("Null")){
            s = String.format("Longitude: %s %f, Latitude: %s %f", this.longitudeDir, Math.abs(this.longitude), this.latitudeDir, Math.abs(this.latitude));
        }
        else {
            s = String.format("Latitude: %s %f", this.latitudeDir, Math.abs(this.latitude));
        }
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LongitudeAndLatitude that = (LongitudeAndLatitude) o;
        return Double.compare(that.longitude, longitude) == 0 &&
                Double.compare(that.latitude, latitude) == 0 &&
                longitudeDir.equals(that.longitudeDir) &&
                latitudeDir.equals(that.latitudeDir);
    }
}
