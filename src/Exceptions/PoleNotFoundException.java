package Exceptions;

public class PoleNotFoundException extends RuntimeException {
    public PoleNotFoundException (String message) {
        super(message);
    }
}
