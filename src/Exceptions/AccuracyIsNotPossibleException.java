package Exceptions;

public class AccuracyIsNotPossibleException extends RuntimeException{
    public AccuracyIsNotPossibleException (String message) { super(message); }
}
