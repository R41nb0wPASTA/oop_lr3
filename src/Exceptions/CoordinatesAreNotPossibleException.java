package Exceptions;

public class CoordinatesAreNotPossibleException extends RuntimeException {
    public CoordinatesAreNotPossibleException (String message) {
        super(message);
    }
}
