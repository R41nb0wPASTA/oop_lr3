import Exceptions.AccuracyIsNotPossibleException;
import Exceptions.CoordinatesAreNotPossibleException;
import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import LongitudeAndLatitude.LongitudeAndLatitude;

public class isHittingPositionTest {
    @Test
    void isHittingAnotherPointWith100AccuracyTest() {
        LongitudeAndLatitude Vlg = LongitudeAndLatitude.Volgograd();
        LongitudeAndLatitude Msc = LongitudeAndLatitude.Moscow();

        boolean exp_res = false;
        boolean res = Vlg.isHittingPosition(Msc, 100);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void isHittingAnotherPointWith0AccuracyTest() {
        LongitudeAndLatitude Vlg = LongitudeAndLatitude.Volgograd();
        LongitudeAndLatitude Msc = LongitudeAndLatitude.Moscow();

        boolean exp_res = true;
        boolean res = Vlg.isHittingPosition(Msc, 0);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void isPointHittingTheSamePointTest() {
        LongitudeAndLatitude Vlg = LongitudeAndLatitude.Volgograd();

        boolean exp_res = true;
        boolean res = Vlg.isHittingPosition(Vlg, 100);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void isHittingAnotherPointWithImpossibleAccuracyTest() {
        LongitudeAndLatitude Vlg = LongitudeAndLatitude.Volgograd();

        boolean exp_res = true;
        boolean res = false;

        try {
            res = Vlg.isHittingPosition(Vlg, -1);
        }
        catch (AccuracyIsNotPossibleException e){
            res = true;
        }

        Assert.assertEquals(exp_res, res);
    }
}
