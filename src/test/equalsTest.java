import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import LongitudeAndLatitude.LongitudeAndLatitude;

public class equalsTest {
    @Test
    void areTrullyEqualsBothWaysTest() {
        LongitudeAndLatitude one = LongitudeAndLatitude.Moscow();
        LongitudeAndLatitude two = new LongitudeAndLatitude(37.61556, "East", 55.75222, "North");

        boolean exp_res = true;
        boolean res1 = one.equals(two);
        boolean res2 = two.equals(one);

        Assert.assertEquals(exp_res, res1);
        Assert.assertEquals(exp_res, res2);
    }

    @Test
    void almostEqualsTest() {
        LongitudeAndLatitude one = LongitudeAndLatitude.Moscow();
        LongitudeAndLatitude two = new LongitudeAndLatitude(37.61557, "East", 55.75222, "North");
        LongitudeAndLatitude three = new LongitudeAndLatitude(-37.61556, "West", 55.75222, "North");

        boolean exp_res = false;
        boolean res1 = one.equals(two);
        boolean res2 = one.equals(three);

        Assert.assertEquals(exp_res, res1);
        Assert.assertEquals(exp_res, res2);
    }

    @Test
    void areNotEqualsTest() {
        LongitudeAndLatitude one = LongitudeAndLatitude.Moscow();
        LongitudeAndLatitude two = LongitudeAndLatitude.Volgograd();

        boolean exp_res = false;
        boolean res = one.equals(two);

        Assert.assertEquals(exp_res, res);
    }
}
