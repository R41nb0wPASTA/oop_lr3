import Exceptions.CoordinatesAreNotPossibleException;
import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import LongitudeAndLatitude.LongitudeAndLatitude;

import java.util.Arrays;
import java.util.List;

public class LonAndLatWithoutNEWSTest {
    @Test
    void creatingCorrectLonAndLatTest() {
        LongitudeAndLatitude one = new LongitudeAndLatitude(-180, 90);

        List<Object> exp_res_lon = Arrays.asList(-180.0, "West");
        List<Object> exp_res_lat = Arrays.asList(90.0, "North");

        Assert.assertEquals(exp_res_lon, one.getLongitude());
        Assert.assertEquals(exp_res_lat, one.getLatitude());
    }

    @Test
    void creatingLonAndLatWithImpossibleLonTest() throws CoordinatesAreNotPossibleException{
        boolean res = false;

        try {
            LongitudeAndLatitude one = new LongitudeAndLatitude(-181, 90);
        }
        catch (CoordinatesAreNotPossibleException e){
            res = true;
        }

        boolean exp_res = true;

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void creatingLonAndLatWithImpossibleLatTest() {

        boolean res = false;

        try {
            LongitudeAndLatitude one = new LongitudeAndLatitude(-180, 91);
        }
        catch (CoordinatesAreNotPossibleException e){
            res = true;
        }

        boolean exp_res = true;

        Assert.assertEquals(exp_res, res);
    }
}
