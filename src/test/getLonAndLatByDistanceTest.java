import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import LongitudeAndLatitude.LongitudeAndLatitude;

public class getLonAndLatByDistanceTest {
    @Test
    void getCorrectLonAndLatByDistanceBothWaysTest() {
        LongitudeAndLatitude one = LongitudeAndLatitude.Moscow();
        LongitudeAndLatitude two = LongitudeAndLatitude.Volgograd();

        double distance = 910.8389208506974;
        double bearing1 = 146.24;
        double bearing2 = 331.71;

        LongitudeAndLatitude fromOneToTwo = one.getLonAndLatByDistance(distance, bearing1);
        LongitudeAndLatitude fromTwoToOne = two.getLonAndLatByDistance(distance, bearing2);

        boolean exp_res = true;

        boolean res1 = fromOneToTwo.isHittingPosition(two, 99);
        boolean res2 = fromTwoToOne.isHittingPosition(one, 99);

        Assert.assertEquals(exp_res, res1);
        Assert.assertEquals(exp_res, res2);
    }

    @Test
    void getCorrectLonAndLatByDistanceWithOversizedAndMinusBearingTest() {
        LongitudeAndLatitude Msc = LongitudeAndLatitude.Moscow();
        LongitudeAndLatitude Vlg = LongitudeAndLatitude.Volgograd();

        double distance = 910.8389208506974;
        double bearing1 = 146.24;
        double bearing2 = 146.24 + 180*2;
        double bearing3 = 146.24 - 180*2;

        LongitudeAndLatitude one = Msc.getLonAndLatByDistance(distance, bearing1);
        LongitudeAndLatitude two = Msc.getLonAndLatByDistance(distance, bearing2);
        LongitudeAndLatitude three = Msc.getLonAndLatByDistance(distance, bearing3);

        boolean exp_res = true;

        boolean res1 = Vlg.isHittingPosition(one, 99);
        boolean res2 = Vlg.isHittingPosition(two, 99);
        boolean res3 = Vlg.isHittingPosition(three, 99);

        Assert.assertEquals(exp_res, res1);
        Assert.assertEquals(exp_res, res2);
        Assert.assertEquals(exp_res, res3);
    }
}
