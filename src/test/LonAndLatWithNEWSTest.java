import Exceptions.CoordinatesAreNotPossibleException;
import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import LongitudeAndLatitude.LongitudeAndLatitude;

import java.util.Arrays;
import java.util.List;

public class LonAndLatWithNEWSTest {
    @Test
    void creatingCorrectLonAndLatTest() {
        LongitudeAndLatitude one = new LongitudeAndLatitude(-180, "West", 90, "North");

        List<Object> exp_res_lon = Arrays.asList(-180.0, "West");
        List<Object> exp_res_lat = Arrays.asList(90.0, "North");

        Assert.assertEquals(exp_res_lon, one.getLongitude());
        Assert.assertEquals(exp_res_lat, one.getLatitude());
    }

    @Test
    void creatingLonAndLatWithImpossibleLonTest() throws CoordinatesAreNotPossibleException{
        boolean res = false;

        try {
            LongitudeAndLatitude one = new LongitudeAndLatitude(-181, "West", 90, "North");
        }
        catch (CoordinatesAreNotPossibleException e){
            res = true;
        }

        boolean exp_res = true;

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void creatingLonAndLatWithImpossibleLatTest() {

        boolean res = false;

        try {
            LongitudeAndLatitude one = new LongitudeAndLatitude(-180, "West", 91, "North");
        }
        catch (CoordinatesAreNotPossibleException e){
            res = true;
        }

        boolean exp_res = true;

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void creatingLonAndLatWithIncorrectLonSignTest() throws CoordinatesAreNotPossibleException{
        LongitudeAndLatitude one = new LongitudeAndLatitude(-180, "East", 90, "North");

        Assert.assertEquals(180.0, one.getLongitudeDegreesOnly());
        Assert.assertEquals(90.0, one.getLatitudeDegreesOnly());
    }

    @Test
    void creatingLonAndLatWithIncorrectLatSignTest() throws CoordinatesAreNotPossibleException{
        LongitudeAndLatitude one = new LongitudeAndLatitude(180, "East", 90, "South");

        Assert.assertEquals(180.0, one.getLongitudeDegreesOnly());
        Assert.assertEquals(-90.0, one.getLatitudeDegreesOnly());
    }
}
