import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import LongitudeAndLatitude.LongitudeAndLatitude;

public class toStringTest {
    @Test
    void regularLonAndLatToStringTest() {
        LongitudeAndLatitude one = LongitudeAndLatitude.Moscow();

        String exp_res = "Longitude: East 37,615560, Latitude: North 55,752220";

        Assert.assertEquals(exp_res, one.toString());
    }

    @Test
    void polesLonAndLatToStringTest() {
        LongitudeAndLatitude one = LongitudeAndLatitude.NorthPole();
        LongitudeAndLatitude two = LongitudeAndLatitude.SouthPole();

        String exp_res1 = "Latitude: North 90,000000";
        String exp_res2 = "Latitude: South 90,000000";

        Assert.assertEquals(exp_res1, one.toString());
        Assert.assertEquals(exp_res2, two.toString());
    }
}
