import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import LongitudeAndLatitude.LongitudeAndLatitude;

public class LonAndLatPremadeTest {
    @Test
    void VolgogradTest() {
        LongitudeAndLatitude Vlg = LongitudeAndLatitude.Volgograd();
        LongitudeAndLatitude exp = new LongitudeAndLatitude(44.5168022, "East", 48.7106765, "North");

        Assert.assertEquals(Vlg.getLongitudeDegreesOnly(), exp.getLongitudeDegreesOnly());
        Assert.assertEquals(Vlg.getLatitudeDegreesOnly(), exp.getLatitudeDegreesOnly());
    }

    @Test
    void MoscowTest() {
        LongitudeAndLatitude Msc = LongitudeAndLatitude.Moscow();
        LongitudeAndLatitude exp = new LongitudeAndLatitude(37.61556, "East", 55.75222, "North");

        Assert.assertEquals(Msc.getLongitudeDegreesOnly(), exp.getLongitudeDegreesOnly());
        Assert.assertEquals(Msc.getLatitudeDegreesOnly(), exp.getLatitudeDegreesOnly());
    }

    @Test
    void NorthPoleTest() {
        LongitudeAndLatitude NP = LongitudeAndLatitude.NorthPole();

        Assert.assertEquals(NP.getLongitudeDegreesOnly(), 0.0);
        Assert.assertEquals(NP.getLatitudeDegreesOnly(), 90.0);
    }

    @Test
    void SouthPoleTest() {
        LongitudeAndLatitude SP = LongitudeAndLatitude.SouthPole();

        Assert.assertEquals(SP.getLongitudeDegreesOnly(), 0.0);
        Assert.assertEquals(SP.getLatitudeDegreesOnly(), -90.0);
    }
}
