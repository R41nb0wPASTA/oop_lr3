import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import LongitudeAndLatitude.LongitudeAndLatitude;

public class getDistanceFromTest {
    @Test
    void getSameCorrectDistanceBothWaysTest() {
        LongitudeAndLatitude one = LongitudeAndLatitude.Moscow();
        LongitudeAndLatitude two = LongitudeAndLatitude.Volgograd();

        double exp_res = 912.2125823608645;

        double res1 = one.getDistanceFrom(two);
        double res2 = two.getDistanceFrom(one);

        Assert.assertEquals(exp_res, res1);
        Assert.assertEquals(exp_res, res2);
    }

    @Test
    void getZeroDistanceFromOneCoordinatesToTheSameCoordinatesTest() {
        LongitudeAndLatitude one = LongitudeAndLatitude.Moscow();

        double exp_res = 0.0;

        double res = one.getDistanceFrom(one);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void getCorrectDistanceFromPolesToCoordinates() {
        LongitudeAndLatitude one = LongitudeAndLatitude.Volgograd();
        LongitudeAndLatitude NorthPole = LongitudeAndLatitude.NorthPole();
        LongitudeAndLatitude SouthPole = LongitudeAndLatitude.SouthPole();

        int exp_res_North = 4591;
        int exp_res_South = 15423;

        int res_North1 = (int)one.getDistanceFrom(NorthPole);
        int res_North2 = (int)NorthPole.getDistanceFrom(one);

        int res_South1 = (int)one.getDistanceFrom(SouthPole);
        int res_South2 = (int)SouthPole.getDistanceFrom(one);

        Assert.assertEquals(exp_res_North, res_North1);
        Assert.assertEquals(exp_res_North, res_North2);
        Assert.assertEquals(exp_res_South, res_South1);
        Assert.assertEquals(exp_res_South, res_South2);
    }
}
